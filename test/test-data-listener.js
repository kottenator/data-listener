import test from 'ava';
import DataListener from '../lib/data-listener';

test("Data object transformation", t => {
  t.plan(10);

  let original = {x: 1};
  let wrapped = new DataListener(original);

  t.true(wrapped.dataListener instanceof DataListener);
  t.true(wrapped.eventManager instanceof EventManager);
  t.true(typeof wrapped.on === 'function');
  t.true(typeof wrapped.off === 'function');
  t.true(typeof wrapped.trigger === 'function');

  // Original object has the properies too. However, this is just a side-effect,
  // don't modify the original object, it will not trigger any events!
  t.is(original.dataListener, wrapped.dataListener);
  t.is(original.eventManager, wrapped.eventManager);
  t.is(original.on, wrapped.on);
  t.is(original.off, wrapped.off);
  t.is(original.trigger, wrapped.trigger);
});

test("Nested object transformation", t => {
  t.plan(10);

  let data = new DataListener({a: {x: 1}});

  t.true(data.a.dataListener instanceof DataListener);
  t.true(data.a.eventManager instanceof EventManager);
  t.true(typeof data.a.on === 'function');
  t.true(typeof data.a.off === 'function');
  t.true(typeof data.a.trigger === 'function');

  data.b = {y: 2};

  t.true(data.b.dataListener instanceof DataListener);
  t.true(data.b.eventManager instanceof EventManager);
  t.true(typeof data.b.on === 'function');
  t.true(typeof data.b.off === 'function');
  t.true(typeof data.b.trigger === 'function');
});

test("Listen to a specific property change", t => {
  t.plan(4);

  let theKing = new DataListener({
    firstName: "Jon",
    lastName: "Snow"
  });

  theKing.on('prop-change:lastName', (data, prop, from, to) => {
    t.is(data, theKing);
    t.is(prop, 'lastName');
    t.is(from, 'Snow');
    t.is(to, 'Stark');
  });

  theKing.firstName = "Ned";
  theKing.lastName = "Stark";
});

test("Listen to any property change", t => {
  t.plan(4);

  let theKing = new DataListener({
    firstName: "Jon",
    lastName: "Snow"
  });

  theKing.on('prop-change', (data, prop, from, to) => {
    if (prop === 'firstName') {
      t.is(from, 'Jon');
      t.is(to, 'Ned');
    } else if (prop === 'lastName') {
      t.is(from, 'Snow');
      t.is(to, 'Stark');
    }
  });

  theKing.firstName = "Ned";
  theKing.lastName = "Stark";
});

test("Listen to a specific property deletion", t => {
  t.plan(7);

  let data = new DataListener({name: 'X'});

  data.on('prop-delete:name', (obj, prop, value) => {
    t.is(obj, data);
    t.is(prop, 'name');
    t.is(value, 'X');
  });

  // Property deletion event also triggers the property change event.
  data.on('prop-change:name', (obj, prop, from, to) => {
    t.is(obj, data);
    t.is(prop, 'name');
    t.is(from, 'X');
    t.is(to, undefined);
  });

  delete data.name;
});

test("Listen to any property deletion", t => {
  t.plan(4);

  let data = new DataListener({x: 1, y: 2});
  let expectedProp, expectedValue;

  data.on('prop-delete', (data, prop, value) => {
    t.is(prop, expectedProp);
    t.is(value, expectedValue);
  });

  [expectedProp, expectedValue] = ['x', 1];

  delete data.x;

  [expectedProp, expectedValue] = ['y', 2];

  delete data.y;
});

test("Try to delete not existing property", t => {
  let data = new DataListener({x: 1});

  data.on('prop-delete prop-change', () => t.fail());

  // No error should occur, no event should be triggered, `true` should be returned.
  t.true(delete data.y);
});

test("Listen to a specific property added", t => {
  t.plan(3);

  let data = new DataListener({});

  data.on('prop-change:a', (data, prop, from, to) => {
    t.is(prop, 'a');
    t.is(from, undefined);
    t.is(to, 42);
  });

  data.a = 42;
});

test("Listen to a specific property added after deletion", t => {
  t.plan(4);

  let data = new DataListener({prop: 'X'});

  data.on('prop-change:prop', (data, prop, from, to) => {
    t.is(from, expectedFrom);
    t.is(to, expectedTo);
  });

  let expectedFrom = 'X';
  let expectedTo = undefined;

  delete data.prop;

  expectedFrom = undefined;
  expectedTo = 'Y';

  data.prop = 'Y';
});

test("Listen to a nested object property change", t => {
  t.plan(6);

  let data = new DataListener({a: {x: 1}});

  data.a.on('prop-change:x', (data, prop, from, to) => {
    t.is(prop, 'x');
    t.is(from, 1);
    t.is(to, 2);
  });

  data.a.x = 2;
  data.b = {y: 3};

  data.b.on('prop-change:y', (data, prop, from, to) => {
    t.is(prop, 'y');
    t.is(from, 3);
    t.is(to, 4);
  });

  data.b.y = 4;
});

test("Try to change original object", t => {
  t.plan(1);

  let original = {x: 1};
  let wrapped = new DataListener(original);

  original.on('prop-change', () => t.fail());
  original.x = 2;

  t.pass();
});
