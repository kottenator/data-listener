import logger from './logger.js';

export default class DataListener {
  static register(dataObject) {
    let dataListener;
    if (this.registry.has(dataObject)) {
      dataListener = this.registry.get(dataObject);
      logger.debug("Existing DataListener found:", dataListener, "for data:", dataObject);
    } else {
      dataListener = new this(dataObject);
      logger.debug("New DataListener created:", dataListener, "for data:", dataObject);
      this.registry.set(dataObject, dataListener);
    }
    return dataListener;
  }

  constructor(dataObject) {
    this.data = dataObject;
    this.nodes = [];
    this.proxy = new Proxy(dataObject, {
      set: (...args) => this.setProp(...args),
      deleteProperty: (...args) => this.deleteProp(...args)
    });
  }

  setProp(dataObject, propName, newValue) {
    let oldValue = dataObject[propName];
    logger.debug("Changing property:", propName, "from:", oldValue, "to:", newValue);
    this.nodes.forEach(node => {
      this.matchNode(node, propName, oldValue, newValue, 'change');
    });
    dataObject[propName] = newValue;
    return true;
  }

  deleteProp(dataObject, propName) {
    if (propName in dataObject) {
      let oldValue = dataObject[propName];
      logger.debug("Deleting property:", propName, "last value:", oldValue);
      this.nodes.forEach(node => {
        this.matchNode(node, propName, oldValue, undefined, 'delete');
      });
      delete dataObject[propName];
      return true;
    }
    return false;
  }

  registerNode(node) {
    logger.debug("Registering node:", node, "for DataListener:", this);
    node.dataListener = this;
    this.nodes.push(node);
    let childNode = node.childNode;
    if (childNode && childNode.dataListener) {
      childNode.dataListener.unregisterNode(childNode);
    }
    Object.entries(this.data).forEach(([propName, value]) => {
      this.matchNode(node, propName, value, value, 'init');
    });
  }

  unregisterNode(node) {
    logger.debug("Unregistering node:", node, "for DataListener:", this);
    this.nodes = this.nodes.filter(ownNode => {
      if (ownNode === node) {
        node.dataListener = null;
        let childNode = node.childNode;
        if (childNode && childNode.dataListener) {
          childNode.dataListener.unregisterNode(childNode);
        }
        return false;
      }
      return true;
    });
  }

  matchNode(node, propName, oldValue, newValue, event) {
    logger.debug("Trying to match node:", node, "to property:", propName, "caused by event:", event);
    if (node.matcher(propName) === true) {
      if (node.callback) {
        node.callback(propName, oldValue, newValue, {data: this.data, event});
      }
      if (node.childNode) {
        try {
          let dataListener = this.constructor.register(newValue);
          dataListener.registerNode(node.childNode);
        } catch(e) {
          logger.debug("Failed to get/create a DataListener for data:", newValue);
        }
      }
    }
  }
}

DataListener.registry = new WeakMap();
DataListener.logger = logger;
