import logger from './logger.js';
import DataListener from './data-listener.js';

export default function DataProxy(dataObject) {
  let dataListener = DataListener.register(dataObject);
  dataListener.setProp = DataProxy.setProp;
  let proxy = dataListener.proxy;
  Object.defineProperty(proxy, 'dataListener', {value: dataListener});
  return proxy;
}

DataProxy.setProp = function(dataObject, propName, newValue) {
  DataListener.prototype.setProp.call(this, dataObject, propName, newValue);
  try {
    let dataListener = DataListener.register(newValue);
    dataObject[propName] = dataListener.proxy;
  } catch(e) {
    logger.debug("Failed to get/create a DataListener for data:", newValue);
  }
  return true;
};
