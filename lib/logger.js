const logger = {
  enabled: false,
  debug(...args) {
    if (this.enabled) {
      console.log(...args);
    }
  }
};

export default logger;
