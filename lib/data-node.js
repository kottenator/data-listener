/**
 * @member {function(Object): Array<*>} matcher
 * @member {function(string): boolean} callback
 * @member {[DataNode]} childNode
 * @member {[DataListener]} dataListener
 */
export default class DataNode {
  static path(matchers, callback) {
    let rootNode = null;
    let parentNode = null;
    for (let matcher of matchers) {
      if (typeof matcher === 'string') {
        let expectedPropName = matcher;
        matcher = propName => propName === expectedPropName;
      }
      let node = new this(matcher);
      if (parentNode) {
        parentNode.childNode = node;
      }
      if (!rootNode) {
        rootNode = node;
      }
      parentNode = node;
    }
    if (parentNode) {
      parentNode.callback = callback;
    }
    return rootNode;
  }

  constructor(matcher, callback = null, childNode = null) {
    this.matcher = matcher;
    this.callback = callback;
    this.childNode = childNode;
    this.dataListener = null;
  }
}
