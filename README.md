# Data DataListener

Event listener for data changes. Designed to listen nested data.

Uses ES2015 `Proxy` under the hood.

## Install

Clone/download this repo _for now_.

## Use

```js
let theKing = new Listener({
  firstName: "Jon",
  lastName: "Snow"
});

theKing.on('prop-change', (data, prop, from, to) => {
  console.log(`Prop ${prop} changed from ${from} to ${to}`);
});

theKing.on('prop-change:lastName', (data, prop, from, to) => {
  console.log(`King's last name changed from ${from} to ${to}`);
});

theKing.firstName = "Ned";
// Prop firstName changed from Jon to Ned
theKing.lastName = "Stark";
// Prop lastName changed from Snow to Stark
// King's last name changed from Snow to Stark
theKing.address = "Westeros";
// Prop address changed from undefined to Westeros
delete theKing.address;
// Prop address changed from Westeros to undefined

// TODO - nested objects
theKing.on('prop-change:address.city.population', (data, prop, from, to) => {
  console.log(`City population changed from ${from} to ${to}`);
});

theKing.address = {city: {name: "Winterfell", population: 1000}}
// City population changed from undefined to 1000
theKing.address.city.population = 50;
// City population changed from 1000 to 50
```
